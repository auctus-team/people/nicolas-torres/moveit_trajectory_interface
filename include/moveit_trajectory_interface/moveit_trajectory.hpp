#pragma once

#include "pinocchio/fwd.hpp"
#include "pinocchio/parsers/urdf.hpp"
#include "pinocchio/algorithm/jacobian.hpp"
#include "pinocchio/algorithm/joint-configuration.hpp"
#include "pinocchio/algorithm/kinematics.hpp"
#include "pinocchio/algorithm/frames.hpp"
#include "pinocchio/multibody/model.hpp"
#include "pinocchio/algorithm/model.hpp"


#include <ros/ros.h>
#include <ros/node_handle.h>

// MoveIt

#include <moveit_trajectory_interface/getPath.h>
#include <moveit_trajectory_interface/progress.h>
#include <actionlib/server/simple_action_server.h>
#include <moveit_trajectory_interface/TrajectoryAction.h>
#include <realtime_tools/realtime_publisher.h>
#include <geometry_msgs/TwistStamped.h>
#include <geometry_msgs/PoseStamped.h>
#include <std_srvs/Empty.h>
#include <eigen_conversions/eigen_msg.h>
#include <toppra_extensions/toppra/constrained_trajectory.h>
#include <chrono>

class MoveItTrajectory
{
  public:
    MoveItTrajectory(){};
    void init(std::string robot_description_param_name,Eigen::VectorXd q_init, std::vector<std::string> joint_names =  std::vector<std::string>() );
    void stop();
    bool load_robot_model(std::string robot_description, std::vector<std::string> joint_names = std::vector<std::string>());

    void setControlledFrame(std::string controlled_frame);
    /**
     * \brief
     * Get the desired Pose
     */
    Eigen::Affine3d getCartesianPose();

    /**
     * \brief
     * Get the desired Twist
     */
    Eigen::Matrix<double,6,1> getCartesianVelocity();

    /**
     * \brief
     * Get the desired Acceleration
     */
    Eigen::Matrix<double,6,1> getCartesianAcceleration();

    Eigen::VectorXd getJointAcceleration();

    Eigen::VectorXd getJointVelocity();
    
    Eigen::VectorXd getJointConfiguration();

    Eigen::Affine3d getCartesianPoseAtTime(double time);

    Eigen::VectorXd getCartesianVelocityAtTime(double time);

    Eigen::VectorXd getCartesianAccelerationAtTime(double time);

    Eigen::VectorXd getJointConfigurationAtTime(double time);

    Eigen::VectorXd getJointVelocityAtTime(double time);

    Eigen::VectorXd getJointAccelerationAtTime(double time);
    bool computeNewTrajectory(moveit_msgs::RobotTrajectory path, double max_velocity_scaling_factor, double max_acceleration_scaling_factor);
    void updateTrajectory();
    void publish_trajectory();
    double getTimeProgress();
    void setTrajectoryTimeIncrement(double time_increment);
    bool isInitialized();
    std::string getRobotName();
    void sortJointState(moveit_msgs::RobotTrajectory& msg);
    bool isTrajectoryBuild();
  private:
    template<typename T>
    bool is_in_vector(const std::vector<T> & vector, const T & elt)
    {
      return vector.end() != std::find(vector.begin(),vector.end(),elt);
    }

    ros::NodeHandle node_handle;
    // Model information
    pinocchio::Model model; /*!< @brief pinocchio model*/ 
    pinocchio::Data data; /*!< @brief pinocchio data*/
    std::string controlled_frame;
    
    //Trajectory parameters
    double t_traj = 0.0;
    Eigen::Affine3d cartesian_pose;
    Eigen::Matrix<double,6,1> cartesian_velocity;
    Eigen::Matrix<double,6,1> cartesian_acceleration;
    Eigen::VectorXd joint_configuration ;
    Eigen::VectorXd joint_velocity ;
    Eigen::VectorXd joint_acceleration ;
    bool trajectory_is_build = false;

    // ROS Publishers
    realtime_tools::RealtimePublisher<geometry_msgs::PoseStamped> pose_des_publisher;
    realtime_tools::RealtimePublisher<geometry_msgs::TwistStamped> vel_des_publisher,acc_des_publisher;
    geometry_msgs::Twist cartesian_velocity_msg,cartesian_acceleration_msg;
    geometry_msgs::Pose cartesian_pose_msg;
    moveit_trajectory_interface::progress trajectory_progress;
    double time_increment = 0.001;
    bool control_frame_set = false;
    bool is_initialized = false;
    std::string planning_group;
    std::string robot_description_param_name;
    std::unique_ptr<toppra::ConstrainedTrajectory> toppra_trajectory;
    bool joints_are_unordered = false;
    
};