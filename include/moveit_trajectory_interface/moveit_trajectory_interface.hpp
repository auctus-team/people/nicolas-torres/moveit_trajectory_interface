#pragma once

#include "moveit_trajectory_interface/moveit_trajectory.hpp"
#include "moveit_trajectory_interface/moveit_trajectory_action_server.hpp"

class TrajectoryInterface
{
  public:
  TrajectoryInterface(){
    interface = std::shared_ptr<MoveItTrajectory>(new MoveItTrajectory);
    boost::thread t1(&TrajectoryInterface::action_thread,this,interface);  
  }
  void action_thread(std::shared_ptr<MoveItTrajectory> interface);
  std::shared_ptr<MoveItTrajectory> interface;

};