/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2012, Willow Garage, Inc.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of Willow Garage nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#include "moveit_trajectory_interface/moveit_trajectory.hpp"


void MoveItTrajectory::init( std::string robot_description_param_name,Eigen::VectorXd q_init, std::vector<std::string> joint_names)
{
  this->robot_description_param_name = robot_description_param_name;
  // ROS_WARN("Received new trajectory");
  //   // The :planning_interface:`MoveGroupInterface` class can be easily
  //   // setup using just the name of the planning group you would like to control and plan for.
  std::string robot_description;
  ros::param::get(robot_description_param_name, robot_description);
  load_robot_model(robot_description,joint_names);
  ros::NodeHandle node_handle("/"+model.name);
  pinocchio::forwardKinematics(model,data,q_init);
  pinocchio::updateFramePlacements(model,data);

  if (!control_frame_set)
    controlled_frame = model.frames[model.nframes-1].name;
  // ROS_WARN_STREAM("controlled_frame " << controlled_frame);
  // ROS_WARN_STREAM("q_init " << q_init.transpose());
  cartesian_pose.matrix() = data.oMf[model.getFrameId(controlled_frame)].toHomogeneousMatrix();
  // ROS_WARN_STREAM("cartesian_pose.matrix() " << cartesian_pose.matrix());
  cartesian_velocity = Eigen::MatrixXd::Zero(6,1);
  cartesian_acceleration = Eigen::MatrixXd::Zero(6,1);
  joint_configuration = q_init;
  joint_velocity.resize(model.nv);
  joint_acceleration.resize(model.nv);
  pose_des_publisher.init(node_handle, "cartesian_pose", 1);
  vel_des_publisher.init(node_handle, "vel_traj", 1);
  acc_des_publisher.init(node_handle, "acc_traj", 1);
  is_initialized = true;
}


bool MoveItTrajectory::isInitialized()
{
  return is_initialized;
}

std::string MoveItTrajectory::getRobotName()
{
  return model.name;
}

bool MoveItTrajectory::load_robot_model(std::string robot_description, std::vector<std::string> joint_names)
{
    pinocchio::Model temp_model;
    pinocchio::urdf::buildModelFromXML(robot_description,temp_model,false);
    if (!joint_names.empty())
    {
        std::vector<pinocchio::JointIndex> list_of_joints_to_keep_unlocked_by_id;
        for(std::vector<std::string>::const_iterator it = joint_names.begin();
            it != joint_names.end(); ++it)
        {
            const std::string & joint_name = *it;
            if(temp_model.existJointName(joint_name))
              list_of_joints_to_keep_unlocked_by_id.push_back(temp_model.getJointId(joint_name));

        }
        
        // Transform the list into a list of joints to lock
        std::vector<pinocchio::JointIndex> list_of_joints_to_lock_by_id;
        for(pinocchio::JointIndex joint_id = 1; joint_id < temp_model.joints.size(); ++joint_id)
        {
            const std::string joint_name = temp_model.names[joint_id];
            if(is_in_vector(joint_names,joint_name))
            continue;
            else
            {
            list_of_joints_to_lock_by_id.push_back(joint_id);
            }
        }
        
        // Build the reduced temp_model from the list of lock joints
        Eigen::VectorXd q_rand = randomConfiguration(temp_model);
        model = pinocchio::buildReducedModel(temp_model,list_of_joints_to_lock_by_id,q_rand);
    }
    data = pinocchio::Data(model);

    return true;
}

void MoveItTrajectory::setTrajectoryTimeIncrement(double time_increment)
{
  this->time_increment = time_increment;
}

bool MoveItTrajectory::isTrajectoryBuild()
{
  return trajectory_is_build;
}

void MoveItTrajectory::updateTrajectory()
{
  if (trajectory_is_build) 
  {
    if (toppra_trajectory->isValid()) 
    {       
      joint_configuration = toppra_trajectory->getPosition(t_traj);
      joint_velocity = toppra_trajectory->getVelocity(t_traj);
      joint_acceleration = toppra_trajectory->getAcceleration(t_traj);
      pinocchio::forwardKinematics(model,data,joint_configuration , joint_velocity,joint_acceleration);
      pinocchio::updateFramePlacements(model,data);
      cartesian_pose.matrix() = data.oMf[model.getFrameId(controlled_frame)].toHomogeneousMatrix();
   
      pinocchio::Motion xd = getFrameVelocity(model, data, model.getFrameId(controlled_frame),pinocchio::ReferenceFrame::WORLD);
      pinocchio::Motion ad = getFrameAcceleration(model, data, model.getFrameId(controlled_frame),pinocchio::ReferenceFrame::WORLD);
      
      cartesian_velocity = xd.toVector();
      cartesian_acceleration = ad.toVector();

      if (t_traj >= toppra_trajectory->getDuration())
      { 
        t_traj = 0.0;
        trajectory_is_build = false;
        cartesian_velocity = Eigen::MatrixXd::Zero(6,1);
        cartesian_acceleration = Eigen::MatrixXd::Zero(6,1);
      }
      else
      {
        // Increase time
        t_traj += time_increment;
      }
    } else {
      ROS_WARN_STREAM_ONCE ("Trajectory generation failed.");
    }
  }
  publish_trajectory();
}

Eigen::Affine3d MoveItTrajectory::getCartesianPoseAtTime(double time)
{
  pinocchio::forwardKinematics(model,data,toppra_trajectory->getPosition(time) , toppra_trajectory->getVelocity(time),toppra_trajectory->getAcceleration(time));
  pinocchio::updateFramePlacements(model,data);
  return cartesian_pose;
}

Eigen::VectorXd MoveItTrajectory::getCartesianVelocityAtTime(double time)
{
  pinocchio::forwardKinematics(model,data,toppra_trajectory->getPosition(time) , toppra_trajectory->getVelocity(time),toppra_trajectory->getAcceleration(time));
  pinocchio::updateFramePlacements(model,data);
  pinocchio::Motion xd = getFrameVelocity(model, data, model.getFrameId(controlled_frame),pinocchio::ReferenceFrame::WORLD);
  
  return xd.toVector();
}

Eigen::VectorXd MoveItTrajectory::getCartesianAccelerationAtTime(double time)
{
  pinocchio::forwardKinematics(model,data,toppra_trajectory->getPosition(time) , toppra_trajectory->getVelocity(time),toppra_trajectory->getAcceleration(time));
  pinocchio::updateFramePlacements(model,data);
  pinocchio::Motion ad = getFrameAcceleration(model, data, model.getFrameId(controlled_frame),pinocchio::ReferenceFrame::WORLD);

  return ad.toVector();
}

Eigen::VectorXd MoveItTrajectory::getJointConfigurationAtTime(double time)
{
  return toppra_trajectory->getPosition(time);
}

Eigen::VectorXd MoveItTrajectory::getJointVelocityAtTime(double time)
{
  return toppra_trajectory->getVelocity(time);
}

Eigen::VectorXd MoveItTrajectory::getJointAccelerationAtTime(double time)
{
    return toppra_trajectory->getAcceleration(time);
}

Eigen::Affine3d MoveItTrajectory::getCartesianPose()
{
  return cartesian_pose;
}

Eigen::Matrix<double,6,1> MoveItTrajectory::getCartesianVelocity()
{
  return cartesian_velocity;
}

Eigen::Matrix<double,6,1> MoveItTrajectory::getCartesianAcceleration()
{
  return cartesian_acceleration;
}

Eigen::VectorXd MoveItTrajectory::getJointConfiguration()
{
  return joint_configuration;
}

Eigen::VectorXd MoveItTrajectory::getJointVelocity()
{
  return joint_velocity;
}

Eigen::VectorXd MoveItTrajectory::getJointAcceleration()
{
  return joint_acceleration;
}

void MoveItTrajectory::setControlledFrame(std::string controlled_frame)
{
  this->controlled_frame = controlled_frame;
  control_frame_set = true;
}

void MoveItTrajectory::sortJointState(moveit_msgs::RobotTrajectory& msg)
{
    // This function is used to sort the joint comming from the /joint_state topic
    // They are order alphabetically while we need them to be order as in the URDF file
    // With use the joint_names variable that defined the joints in the correct order
    moveit_msgs::RobotTrajectory sorted_joint_state = msg;
    std::vector<std::string> joint_names_from_urdf;
    for(pinocchio::JointIndex joint_id = 1; joint_id < (pinocchio::JointIndex)model.njoints; ++joint_id)
    {
      joint_names_from_urdf.push_back(model.names[joint_id]);
    }
    if (joint_names_from_urdf != msg.joint_trajectory.joint_names)
    {
      for ( int num_points = 0 ; num_points< msg.joint_trajectory.points.size(); num_points ++)
      {
        int i = 0;
        for (auto ith_unsorted_joint_name: msg.joint_trajectory.joint_names)
        {
            int j=0;
            for (auto jth_joint_name : joint_names_from_urdf)    
            {
                if(ith_unsorted_joint_name.find(jth_joint_name) != std::string::npos)
                {
                    sorted_joint_state.joint_trajectory.points[num_points].positions[j] = msg.joint_trajectory.points[num_points].positions[i];
                    //  sorted_joint_state.joint_trajectory.joint_names[j] = msg.joint_trajectory.joint_names[i];
                    break;
                }
                else
                {
                    j++;
                }
            }
            i++;
        }
      }
    }
    msg =  sorted_joint_state;
}

bool MoveItTrajectory::computeNewTrajectory(moveit_msgs::RobotTrajectory path,double max_velocity_scaling_factor,double max_acceleration_scaling_factor)
{
  if (path.joint_trajectory.points.size() == 0)
  {
    ROS_WARN( "Empty trajectory not doing anything");
  }
  else
  {
    sortJointState(path);

    std::vector<Eigen::VectorXd> waypoints;
    for (int point = 0 ; point < path.joint_trajectory.points.size() ; point++)
    {
      Eigen::VectorXd waypoint(model.nv);
      for ( int i =0 ; i<waypoint.size() ; ++i)
      {
        waypoint[i] = path.joint_trajectory.points[point].positions[i];
      }
      waypoints.push_back(waypoint);
    }

    Eigen::VectorXd max_acceleration(model.nv);
    max_acceleration.setOnes();
    Eigen::VectorXd max_velocity(model.nv);
    max_velocity.setOnes();
    max_velocity = 0.1* model.velocityLimit;
    const double time_step = 0.01;
    toppra_trajectory.reset(new toppra::ConstrainedTrajectory(waypoints, max_velocity,
                                            max_acceleration, time_step));

    t_traj = 0.0;
    trajectory_is_build = true;
  }
  return true;
}

void MoveItTrajectory::publish_trajectory()
{
  tf::poseEigenToMsg(cartesian_pose,cartesian_pose_msg);

  tf::twistEigenToMsg(cartesian_velocity, cartesian_velocity_msg);
  tf::twistEigenToMsg(cartesian_acceleration, cartesian_acceleration_msg);
  //Publish robot desired pose
  if (pose_des_publisher.trylock())
  {
      geometry_msgs::PoseStamped X_des_stamp;
      pose_des_publisher.msg_.header.stamp = ros::Time::now();
      pose_des_publisher.msg_.header.frame_id = "world";
      pose_des_publisher.msg_.pose = cartesian_pose_msg;
      pose_des_publisher.unlockAndPublish();
  }
  //Publish robot desired velocity
  if (vel_des_publisher.trylock())
  {
      geometry_msgs::PoseStamped vel_des_stamp;
      vel_des_publisher.msg_.header.stamp = ros::Time::now();
      vel_des_publisher.msg_.header.frame_id = "world";
      vel_des_publisher.msg_.twist = cartesian_velocity_msg;
      vel_des_publisher.unlockAndPublish();
  }
  //Publish robot desired acceleration

  if (acc_des_publisher.trylock())
  {
      geometry_msgs::PoseStamped acc_des_stamp;
      acc_des_publisher.msg_.header.stamp = ros::Time::now();
      acc_des_publisher.msg_.header.frame_id = "world";
      acc_des_publisher.msg_.twist = cartesian_acceleration_msg;
      acc_des_publisher.unlockAndPublish();
  }
}

void MoveItTrajectory::stop()
{
  trajectory_is_build = false;
  cartesian_velocity = Eigen::MatrixXd::Zero(6,1);
  cartesian_acceleration = Eigen::MatrixXd::Zero(6,1);
  t_traj = toppra_trajectory->getDuration();
}
double MoveItTrajectory::getTimeProgress()
{
  double time_progress = 0.0;
  if (toppra_trajectory->getDuration() != 0.0)
  {
    time_progress = t_traj/toppra_trajectory->getDuration();
  }
  else
  {
      time_progress = 0.0;
  }
  return time_progress;
}